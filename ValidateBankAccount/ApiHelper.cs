﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSharp;
using Newtonsoft.Json;

namespace ValidationTest
{
    public static class ApiHelper
    {
        public static RestClient GetClient(string apiURL)
        {
            if (string.IsNullOrEmpty(apiURL))
                return null;
            RestClient client = new RestClient(apiURL);
            return client;
        }
        public static RestRequest CreateRequest(string End_Point, Method method, Dictionary<string, string> header, Dictionary<string, string> data)
        {
            if (string.IsNullOrEmpty(End_Point))
                return null;
            RestRequest request = new RestRequest(End_Point, method);
            foreach (var item in header)
                request.AddHeader(item.Key, item.Value);
            if (data != null && data.Count > 0)
                request.AddJsonBody(JsonConvert.SerializeObject(data));
            return request;
        }

        public static string RequestLogger(IRestRequest request, string apiURL, string endPoint)
        {
            
            if (request == null)
                return "";
            RequestInfo requestInfo = new RequestInfo();
            requestInfo.apiUrl = apiURL;
            requestInfo.endPoint = endPoint;
            requestInfo.method = Convert.ToString(request.Method);
            requestInfo.parameters = new List<string>();
            foreach (var item in request.Parameters)
                requestInfo.parameters.Add(item.Name + "=" + item.Value);
            return JsonConvert.SerializeObject(requestInfo, Formatting.Indented);
        }
    }

    public class ResponseContent
    {
        public bool isValid { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string customerFacingMessage { get; set; }
        public string actionCode { get; set; }
        public string fieldReference { get; set; }
        public List<RiskCheckMessage> riskCheckMessages { get; set; }
    }

    public class RiskCheckMessage
    {
        public string type { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string customerFacingMessage { get; set; }
        public string actionCode { get; set; }
        public string fieldReference { get; set; }
    }

    public class RequestInfo
    {
        public string apiUrl { get; set; }
        public string endPoint { get; set; }
        public string method { get; set; }
        public List<string> parameters { get; set; }
    }

}
