﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;

namespace ValidationTest
{
    [Binding]
    public class Hooks
    {
        private static ExtentTest _feature;
        private static ExtentTest _scenario;
        private static AventStack.ExtentReports.ExtentReports Extent = new AventStack.ExtentReports.ExtentReports();
        private static string directory = Directory.GetParent(
            Directory.GetParent(
                Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString()).ToString() + "/";
        private static ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(directory);

        [BeforeTestRun]
        public static void ConfigureReport()
        {
            Extent.AttachReporter(htmlReporter);
        }

        [BeforeFeature]
        public static void CreateFeature(FeatureContext featureContext)
        {
            _feature = Extent.CreateTest<Feature>(featureContext.FeatureInfo.Title);
        }

        [BeforeScenario]
        public static void CreateScenario(ScenarioContext _scenarioContext)
        {
            _scenario = _feature.CreateNode<Scenario>(_scenarioContext.ScenarioInfo.Title);
        }

        [AfterStep]
        public static void InsertReportingSteps()
        {
            switch (ScenarioStepContext.Current.StepInfo.StepDefinitionType)
            {
                case StepDefinitionType.Given:
                    if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                    {
                        _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<Given>("<b>Error </b>").Fail(TestContext.CurrentContext.Result.Message);
                    }
                    else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                    {
                        _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<Given>("").Skip("Skipped");
                    }
                    else
                    {
                        if (ScenarioStepContext.Current.Keys.Count > 0)
                        {
                            if (ScenarioStepContext.Current.ContainsKey("request"))
                            {
                                _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Request\n" + ScenarioStepContext.Current["request"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("response"))
                            {
                                _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Response\n" + ScenarioStepContext.Current["response"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("skip"))
                            {
                                _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text).Skip(ScenarioStepContext.Current["skip"].ToString());
                            }
                        }
                        else
                            _scenario.CreateNode<Given>("<b>Given </b>" + ScenarioStepContext.Current.StepInfo.Text);
                    }
                    break;

                case StepDefinitionType.When:
                    if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                    {
                        _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<When>("<b>Error </b>").Fail(TestContext.CurrentContext.Result.Message);
                    }
                    else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                    {
                        _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<When>("").Skip("Skipped");
                    }
                    else
                    {
                        if (ScenarioStepContext.Current.Keys.Count > 0)
                        {
                            if (ScenarioStepContext.Current.ContainsKey("request"))
                            {
                                _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Request\n" + ScenarioStepContext.Current["request"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("response"))
                            {
                                _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Response\n" + ScenarioStepContext.Current["response"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("skip"))
                            {
                                _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text).Skip(ScenarioStepContext.Current["skip"].ToString());
                            }
                        }
                        else
                            _scenario.CreateNode<When>("<b>When </b>" + ScenarioStepContext.Current.StepInfo.Text);
                    }
                    break;

                case StepDefinitionType.Then:
                    if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                    {
                        _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<Then>("<b>Error </b>").Fail(TestContext.CurrentContext.Result.Message);
                    }
                    else if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Skipped)
                    {
                        _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text);
                        _scenario.CreateNode<Then>("").Skip("Skipped");
                    }
                    else
                    {
                        if (ScenarioStepContext.Current.Keys.Count > 0)
                        {
                            if (ScenarioStepContext.Current.ContainsKey("request"))
                            {
                                _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Request\n" + ScenarioStepContext.Current["request"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("response"))
                            {
                                _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text).Info("Http Response\n" + ScenarioStepContext.Current["response"].ToString());
                            }
                            else if (ScenarioStepContext.Current.ContainsKey("skip"))
                            {
                                _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text).Skip(ScenarioStepContext.Current["skip"].ToString());
                            }
                        }
                        else
                            _scenario.CreateNode<Then>("<b>Then </b>" + ScenarioStepContext.Current.StepInfo.Text);
                    }
                    break;
            }
        }
        [AfterTestRun]
        public static void TearDownReport()
        {
            //Flush report once test completes
            Extent.Flush();
        }


    }
}
