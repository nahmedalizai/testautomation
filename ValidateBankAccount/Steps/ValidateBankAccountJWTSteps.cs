﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace ValidationTest.Steps
{
    [Binding]
    public sealed class ValidateBankAccountJWTSteps
    {
        private readonly ScenarioContext _scenarioContext;
        IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        private RestRequest request = null;
        private RestResponse response = null;
        private Dictionary<string, string> header = null;
        private Dictionary<string, string> data = null;
        private ResponseContent responseContent;

        public ValidateBankAccountJWTSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"a SampleRequest with a valid JWT token")]
        public void GivenASampleRequestWithAValidJWTToken()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                header.Add("X-Auth-Key", config["authKey"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["bankAccount"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [When(@"sample request is posted to api")]
        public void WhenSampleRequestIsPostedToApi()
        {
            try
            {
                RestClient client = ApiHelper.GetClient(config["apiURL"]);
                Assert.IsNotNull(client);
                response = (RestResponse)client.Execute(request);
                _scenarioContext.StepContext.Add("response", JsonConvert.SerializeObject(JsonConvert.DeserializeObject(response.Content)));
                Assert.IsNotNull(response);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns ok")]
        public void ThenApiReturnsOk()
        {
            try
            {
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Given(@"a SampleRequest without a JWT token")]
        public void GivenASampleRequestWithoutAJWTToken()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["bankAccount"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns Authorization has been denied for this request message.")]
        public void ThenApiReturnsAuthorizationHasBeenDeniedForThisRequestMessage()
        {
            try
            {
                responseContent = JsonConvert.DeserializeObject<ResponseContent>(response.Content);
                Assert.AreEqual("Authorization has been denied for this request.", responseContent.message);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

    }
}
