﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;


namespace ValidationTest.Steps
{
    [Binding]
    public sealed class ValidateBankAccountNumberSteps
    {
        private readonly ScenarioContext _scenarioContext;
        IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        private RestRequest request = null;
        private RestResponse response = null;
        private Dictionary<string, string> header = null;
        private Dictionary<string, string> data = null;
        private ResponseContent responseContent;

        public ValidateBankAccountNumberSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"a SampleRequest with a valid IBAN")]
        public void GivenASampleRequestWithAValidIBAN()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                header.Add("X-Auth-Key", config["authKey"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["bankAccount"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch(Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [When(@"sample request for IBAN validation is posted to api")]
        public void WhenSampleRequestForIBANValidationIsPostedToApi()
        {
            try
            {
                RestClient client = ApiHelper.GetClient(config["apiURL"]);
                Assert.IsNotNull(client);
                response = (RestResponse)client.Execute(request);
                _scenarioContext.StepContext.Add("response", JsonConvert.SerializeObject(JsonConvert.DeserializeObject(response.Content)));
                Assert.IsNotNull(response);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns IsValid true")]
        public void ThenApiReturnsIsValidTrue()
        {
            try
            {
                responseContent = JsonConvert.DeserializeObject<ResponseContent>(response.Content);
                Assert.AreEqual(true, responseContent.isValid);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Given(@"a SampleRequest with an invalid IBAN")]
        public void GivenASampleRequestWithAnInvalidIBAN()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                header.Add("X-Auth-Key", config["authKey"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["invalidBankAccount"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns Value format is incorrect message")]
        public void ThenApiReturnsValueFormatIsIncorrectMessage()
        {
            try
            {
                responseContent = JsonConvert.DeserializeObject<ResponseContent>(response.Content);
                Assert.AreEqual("Value format is incorrect.", responseContent.riskCheckMessages[0].message);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Given(@"a SampleRequest with a shorter than limit IBAN")]
        public void GivenASampleRequestWithAShorterThanLimitIBAN()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                header.Add("X-Auth-Key", config["authKey"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["invalidShortIBAN"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns A string value with minimum length seven is required message")]
        public void ThenApiReturnsAStringValueWithMinimumLengthSevenIsRequiredMessage()
        {
            try
            {
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    string content = response.Content;
                    content = content.Replace('[', ' ');
                    content = content.Replace(']', ' ');
                    responseContent = JsonConvert.DeserializeObject<ResponseContent>(content);
                    Assert.AreEqual("A string value with minimum length 7 is required.", responseContent.message);
                }
                else
                    Assert.Fail("Expected StatusCode : BadRequest, Actual:" + response.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Given(@"a SampleRequest with a longer than limit IBAN")]
        public void GivenASampleRequestWithALongerThanLimitIBAN()
        {
            try
            {
                header = new Dictionary<string, string>();
                header.Add("Content-Type", config["contentType"]);
                header.Add("X-Auth-Key", config["authKey"]);
                data = new Dictionary<string, string>();
                data.Add("bankAccount", config["invalidLongIBAN"]);
                request = ApiHelper.CreateRequest(config["endPoint"], Method.POST, header, data);
                _scenarioContext.StepContext.Add("request", ApiHelper.RequestLogger(request, config["apiURL"], config["endPoint"]));
                Assert.IsNotNull(request);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

        [Then(@"Api returns A string value exceeds maximum length of thirtyfour message")]
        public void ThenApiReturnsAStringValueExceedsMaximumLengthOfThirtyfourMessage()
        {
            try
            {
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    string content = response.Content;
                    content = content.Replace('[', ' ');
                    content = content.Replace(']', ' ');
                    responseContent = JsonConvert.DeserializeObject<ResponseContent>(content);
                    Assert.AreEqual("A string value exceeds maximum length of 34.", responseContent.message);
                }
                else
                    Assert.Fail("Expected StatusCode : BadRequest, Actual:" + response.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expcetion : " + ex.Message);
            }
        }

    }
}
