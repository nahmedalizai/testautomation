﻿Feature: ValidateBankAccountNumber
	Check if Bank Account Number validation is working properly

@integrationTest
Scenario: Valid IBAN
	Given a SampleRequest with a valid IBAN
	When sample request for IBAN validation is posted to api
	Then Api returns IsValid true

@integrationTest
Scenario: Invalid IBAN
	Given a SampleRequest with an invalid IBAN
	When sample request for IBAN validation is posted to api
	Then Api returns Value format is incorrect message

@integrationTest
Scenario: IBAN Length less than minimum
	Given a SampleRequest with a shorter than limit IBAN
	When sample request for IBAN validation is posted to api
	Then Api returns A string value with minimum length seven is required message

@integrationTest
Scenario: IBAN Length more than maximum
	Given a SampleRequest with a longer than limit IBAN
	When sample request for IBAN validation is posted to api
	Then Api returns A string value exceeds maximum length of thirtyfour message