﻿Feature: ValidateBankAccount-JWT
	Check if Bank Account JWT validation is working properly

@integrationTest
Scenario: Positive
	Given a SampleRequest with a valid JWT token
	When sample request is posted to api
	Then Api returns ok

@integrationTest
Scenario: Negative
	Given a SampleRequest without a JWT token
	When sample request is posted to api
	Then Api returns Authorization has been denied for this request message.